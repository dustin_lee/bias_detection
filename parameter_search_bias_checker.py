import pandas as pd
import torch

import models
import utils


def preprocess(data, embedder, sentiment_analyzer):
    output = []
    for training_split in data:
        training_split_data = []
        for bias_split in training_split:
            bias_split_data = []
            for sentence, target in bias_split:
                sentence_embedded = embedder(sentence).squeeze(1)
                sentiment = sentiment_analyzer(sentence).squeeze(0)
                sample = ((sentence_embedded, sentiment), target)
                bias_split_data.append(sample)
            training_split_data.append(bias_split_data)
        output.append(training_split_data)
    return output


def stack_fn(samples):
  sentences = []
  sentiments = []
  for sentence, sentiment in samples:
    sentences.append(sentence)
    sentiments.append(sentiment)
  sentences = torch.nn.utils.rnn.pad_sequence(sentences)
  sentiments = torch.stack(sentiments)
  return (sentences, sentiments)


def parameters_fn():
    output = []
    for hidden_size in [4, 6, 8, 12, 16, 24, 32, 48]:
        for num_layers in [2, 4]:
            for bidirectional in [True]:
                for dim_feedforward in [4, 6, 8, 16, 32]:
                    for nhead in [2, 4, 6]:
                        if (dim_feedforward%nhead == 0
                            and hidden_size%nhead == 0):
                            parameters = (hidden_size,
                                          num_layers,
                                          bidirectional,
                                          dim_feedforward,
                                          nhead)
                            output.append(parameters)
    return output


def main(data, embedder, sentiment_analyzer):
    data_preprocessed = preprocess(data, embedder, sentiment_analyzer)
    data_train_preprocessed, data_test_preprocessed = data_preprocessed

    loader_train = utils.Loader(data_train_preprocessed, batch_size=32,
                                even_proportions=True,
                                bucket_key=(lambda x: x[0].shape[0]),
                                stack_fn=stack_fn)
    loader_test = utils.Loader(data_test_preprocessed, batch_size=64,
                               bucket_key=(lambda x: x[0].shape[0]),
                               stack_fn=stack_fn)

    parameters_best = ()
    f1_best = 0
    for parameters in parameters_fn():
        bias_checker = models.Bias_Checker(embedder.word_embedding_len,
                                           sentiment_analyzer.sentiment_len,
                                           *parameters)
        utils.train(bias_checker, loader_train,
                    num_epoches=30, show_progress=False)
        f1, _ = utils.test(bias_checker, loader_test)
        print(f'The F1 score for the parameters {parameters} '
              + f'is {round(f1, 2)}.')
        if f1 > f1_best:
            f1_best = f1
            parameters_best = parameters
            pd.Series(parameters).to_pickle('hyperparameters_bias_checker.plk')
            torch.save(bias_checker.state_dict(), 'parameters_bias_checker.pt')

    print(f'The best F1 is {round(f1_best, 2)} '
          + f'with the parameters {parameters_best}.')

    return None
