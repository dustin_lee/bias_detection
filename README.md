Dustin Enyeart
<span style="font-variant:small-caps;">ece</span> 570 (Artificial Intelligence)
Purdue University
[Git Link](https://Dustin_Enyeart@bitbucket.org/Dustin_Enyeart/ece-570-project-bias-detection-in-wikipedia.git)


## How to Run the Code

Once the dependencies are installed, you can **just run the cells in the jupyter notebook main.ipynb**. To install the dependencies, run the command

> pip install -r requirements.txt

to install the required libraries, run the command

> python -m spacy download en_core_web_sm

to download the the SpaCy model that is used, and run the command

> git clone https://github.com/ChristophHubeL3S/Neural_Based_Statement_Classification_for_Biased_Language_WSDM2019

to get the data. If Python 3 is not the default version of Python on the system, replace *pip* and *python* with *pip3* and *python3* in these commands, respectively. It is recommended to use a virtual environment.


---

## Notes

All code in this folder is original. The data is from the paper [*Neural based statement classification for biased language*](https://dl.acm.org/doi/pdf/10.1145/3289600.3291018) by Hube and Fetahu.
