import math as ma
import os
import random as rand

import pandas as pd
import torch
import torch.nn.functional as F


def data_from_file(file, bias):
    #This is used in get_data.
    data = []
    dir = 'Neural_Based_Statement_Classification_for_Biased_Language_WSDM2019'
    with open(os.path.join(dir, file)) as temp:
        for line in temp:
            line = line.strip()
            data.append((line, bias))
    return data


def get_data(testing_proportion=.85, print_stats=True):
    '''
    This returns the data into the form (data_train, data_test),
    where data_train is of the form (data_train_neutral, data_train_biased)
    and data_test is of the form (data_test_neutral, data_test_biased).
    '''
    data_neutral = data_from_file('statements_neutral_featured',
                                  torch.Tensor([0.]))
    data_biased = data_from_file('statements_biased',
                                 torch.Tensor([1.]))

    n = int(testing_proportion*len(data_neutral))
    data_neutral_train = data_neutral[:n]
    data_neutral_test = data_neutral[n:]
    n = int(testing_proportion*len(data_biased))
    data_biased_train = data_biased[:n]
    data_biased_test = data_biased[n:]

    if print_stats == True:
        num_samples_train = len(data_neutral_train) + len(data_biased_train)
        print(f'There are {num_samples_train} samples in the training data, '
              + f'and {round(100*len(data_biased_train)/num_samples_train, 1)}'
              + '% of them are biased.')
        num_samples_test = len(data_neutral_test) + len(data_biased_test)
        print(f'There are {num_samples_test} samples in the testing data, and'
              + f' {round(100*len(data_biased_test)/num_samples_test, 1)}% '
              + 'of them are biased.')

    data_train = (data_neutral_train, data_biased_train)
    data_test = (data_neutral_test, data_biased_test)

    return data_train, data_test


class Loader:
    '''
    This loader is able to both bucket the data and to give the same
    number of neutral samples as biased samples per epoch.

    To bucket data is to create batches that consist of samples of similar
    length. This reduces the effects of padding. The parameter bucket_key in
    the intialization is used to decided how to sort the samples into
    buckets. This is needed because it is different for the raw data than for
    the data after it is preprocessed to trian. If the bucket key is None,
    then bucketing does not occur.

    Since there are many more neutral sentences than biased sentences in
    the data, giving the same number of neutral sentences as biased
    samples per epoch prevents the model from learning that it can
    do well early on by just classifying everyhing as biased.
    Setting the paramter even_proportions to True in the initialization
    function makes the loader give the same number of neutral sentences as
    biased sentences. The loader still goes through all neutral sentences
    after a few epoches.

    The stack_fn paramter decided how to combine the samples into a batch.
    This is needed because it is different for the raw data than for the
    data after it is preprocessed to train.
    '''
    def __init__(self, data, batch_size=16,
                 even_proportions=False, bucket_key=None,
                 stack_fn=(lambda x: x)):
        self.data_neutral, self.data_biased = data
        self.batch_size = batch_size
        self.even_proportions = even_proportions
        self.bucket_key = bucket_key
        self.stack_fn = stack_fn

        if even_proportions == True:
            self.num_samples_per_epoch = (2*min(len(self.data_neutral),
                                                len(self.data_biased)))
            self.idx_neutral = 0
            self.idx_biased = 0
        else:
            self.num_samples_per_epoch = (len(self.data_neutral)
                                          + len(self.data_biased))

        self.max_idx_batch = ma.ceil(self.num_samples_per_epoch/self.batch_size)

        return None

    def new_epoch(self):
        self.idx_batch = -1

        #This gets the data for the epoch.
        if self.even_proportions == True:
            self.data = []
            for _ in range(int(self.num_samples_per_epoch/2)):
                self.data.append(self.data_neutral[self.idx_neutral])
                self.data.append(self.data_biased[self.idx_biased])
                self.idx_neutral += 1
                self.idx_biased += 1
                if self.idx_neutral == len(self.data_neutral):
                    self.idx_neutral = 0
                if self.idx_biased == len(self.data_biased):
                    self.idx_biased = 0
        else:
            self.data = self.data_neutral + self.data_biased

        rand.shuffle(self.data)

        if self.bucket_key != None:
            key = (lambda x: self.bucket_key(x[0]))
            self.data.sort(key=key)

        batches = []
        samples = []
        targets = []
        for i, (sample, target) in enumerate(self.data, 1):
            samples.append(sample)
            targets.append(target)
            if i%self.batch_size == 0 or i == len(self.data):
                targets = torch.stack(targets)
                samples = self.stack_fn(samples)
                batch = (samples, targets)
                batches.append(batch)
                samples = []
                targets = []

        if self.bucket_key != None:
            rand.shuffle(batches)

        self.batches = batches

        return None

    def __len__(self):
        return self.max_idx_batch

    def __iter__(self):
        self.new_epoch()
        return self

    def __next__(self):
        self.idx_batch += 1
        if self.idx_batch == self.max_idx_batch:
            raise StopIteration
        else:
            return self.batches[self.idx_batch]


def train(classifier, data, num_epoches=1, show_progress=True):
    optimizer = torch.optim.Adam(classifier.parameters(), lr=1e-3)
    classifier.train()

    for i in range(num_epoches):
        for samples, targets in data:
            optimizer.zero_grad()
            predictions = classifier(samples)
            loss = F.binary_cross_entropy(predictions, targets)
            loss.backward()
            optimizer.step()
        if show_progress == True:
            print(f'Epoch {i+1} is complete. The estimated loss is '
                  + f'{round(loss.item(), 2)}.')

    return None


def test(classifier, data, cutoffs=[]):
    classifier.eval()

    results = []
    with torch.no_grad():
        for samples, targets in data:
            probs = list(classifier(samples))
            targets = list(targets)
            for prob, target in zip(probs, targets):
                results.append([prob.item(), int(target.item())])

    if type(cutoffs) != list and type(cutoffs) != tuple:
        cutoffs = [cutoffs]

    #The cutoff .5 needs to be included to give the f1 score of the model.
    if .5 in cutoffs:
        remove_pt_5 = False
    else:
        cutoffs.append(.5)
        remove_pt_5 = True

    stats = []
    for cutoff in cutoffs:
        n = 0 #The number correct of correct classifications.
        m = 0 #The number of true positives.
        k = 0 #The number of false positives.
        for result in results:
            prob, target = result
            if prob > cutoff:
                prediction = 1.
            else:
                prediction = 0.
            if prediction == target: #It is correctly classified.
                n += 1
            if prediction == 1.:
                if target == 1.: #It is a true positive.
                    m += 1
                else:
                    k += 1 #It is a false positive.
        accuracy = n/len(results)
        num_biased_sentences = sum([target for _, target in results])
        true_pos_rate = m/num_biased_sentences
        false_pos_rate = k/(len(results) - num_biased_sentences)
        try:
            precision = m/(m+k)
        except ZeroDivisionError:
            precision = 1.
        try:
            f1 = 2./(1./true_pos_rate + 1./precision)
        except ZeroDivisionError:
            f1 = 0.
        if cutoff == .5:
            final_f1_score = f1
        if cutoff != .5 or remove_pt_5 == False:
            stats.append([cutoff, accuracy, true_pos_rate, false_pos_rate,
                      precision, f1])
    columns = ['cutoff', 'accuracy', 'true-positive rate',
               'false-positive rate', 'precision', 'F1']
    df = pd.DataFrame(stats, columns=columns)
    df = df.round(2)
    return final_f1_score, df


def display_misclassifications(classifier, data, n1=3, n2=3):
    print('Missclassified sentences:\n')
    m1 = 0 #The number displayed
    m2 = 0
    for sentences, targets in data:
        for sentence, target in zip(sentences, targets):
            target = target.item()
            if abs(classifier(sentence) - target) > .5:
                if target == 0.:
                    if m1 < n1:
                        print(f'The sentence "{sentence}" was incorrectly'
                              + 'labeled as biased.\n')
                        m1 += 1
                else:
                    if m2 < n2:
                        print(f'The sentence "{sentence}" was incorrectly'
                              + 'labeled as neutral.\n')
                        m2 += 1
            if m1 >= n1 and m2 >= n2:
                return None
    return None
